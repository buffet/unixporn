from st3m.application import Application, ApplicationContext
import st3m.run
import leds
import uos

class ProfilePic(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # paint image
        try:
            uos.stat("/flash/profile.png")
            ctx.image(f"/flash/profile.png", -120, -120, 241, 241)
        except OSError:
            ctx.image(f"/flash/sys/apps/profile-pic/profile.png", -120, -120, 241, 241)

        # colorful petals
        leds.set_brightness(10)
        for i in range(40):
            if i % 8 == 0:
                # paint all tips 
                leds.set_rgb(i, 0, 0, 0)   
            elif i // 8 == 0:
                # yellow
                leds.set_rgb(i, 255, 255, 0)
            elif i // 8 == 1:
                # pink
                leds.set_rgb(i, 255, 0, 200)
            elif i // 8 == 2:
                # violet
                leds.set_rgb(i, 153, 50, 204)
            elif i // 8 == 3:
                # cyan
                leds.set_rgb(i, 0, 255, 255)
            elif i // 8 == 4:
                # green
                leds.set_rgb(i, 0, 255, 0)   
        

        leds.update()



    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing


if __name__ == '__main__':
    st3m.run.run_view(ProfilePic(ApplicationContext()))
